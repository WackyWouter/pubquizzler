<?php 
class MultipleChoice extends Question{
    public $questionOptAnswers;
    public $questionRightAnswer;

    function __construct($questionTitle, $questionImageBool, $questionOptAnswers, $questionRightAnswer)
    {
        parent::__construct($questionTitle, $questionImageBool);
        $this->questionOptAnswers = $questionOptAnswers;
        $this->questionRightAnswer = $questionRightAnswer;
    }

    function get_questionTextAnswer(){
        return $this->questionRightAnswer;
    }

    function get_questionOptAnswers(){
        return $this->questionOptAnswers;
    }
}

?>