<?php
class Question {

    public $questionTitle;
    public $questionImageBool;
    public $questionImage;


    function __construct($questionTitle, $questionImageBool) {
        $this->questionTitle = $questionTitle;
        $this->questionImageBool = $questionImageBool;
    }

    function set_questionImage($questionImage){
        $this->questionImage = $questionImage;
    }

    function get_questionTitle(){
        return $this->questionTitle;
    }

    function get_questionImageBool(){
        return $this->questionImageBool;
    }

    function get_questionImage(){
        return $this->questionImage;
    }


}

?>