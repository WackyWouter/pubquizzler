<?php 
class BoolQuestion extends Question{
    public $questionTextAnswer;

    function __construct($questionTitle, $questionImageBool, $questionTextAnswer)
    {
        parent::__construct($questionTitle, $questionImageBool);
        $this->questionTextAnswer = $questionTextAnswer;
    }

    function get_questionTextAnswer(){
        return $this->questionTextAnswer;
    }
}

?>