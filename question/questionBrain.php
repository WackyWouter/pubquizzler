<?php
require 'multipleChoice.php';
require 'textQuestion.php';

class QuestionBrain{
    protected $questionNumber = 0;
    protected $sectionNumber = 0;

    protected $boolQuestionBank = [
        new BoolQuestion('Some cats are actually allergic to humans', false, true),
        new BoolQuestion('You can lead a cow down stairs but not up stairs.', false, false),
        new BoolQuestion('Approximately one quarter of human bones are in the feet.', false, true),
        new BoolQuestion('A slug\'s blood is green.', false, true),
        new BoolQuestion('Buzz Aldrin\'s mother\'s maiden name was \"Moon\".', false, true),
        new BoolQuestion('It is illegal to pee in the Ocean in Portugal.', false, true),
        new BoolQuestion('No piece of square dry paper can be folded in half more than 7 times.', false, false),
        new BoolQuestion('In London, UK, if you happen to die in the House of Parliament, you are technically entitled to a state funeral, because the building is considered too sacred a place.', false, true),
        new BoolQuestion('The loudest sound produced by any animal is 188 decibels. That animal is the African Elephant.', false, false),
        new BoolQuestion('The total surface area of two human lungs is approximately 70 square metres.', false, true),
        new BoolQuestion('Google was originally called \"Backrub\".', false, true),
        new BoolQuestion('Chocolate affects a dog\'s heart and nervous system; a few ounces are enough to kill a small dog.', false, true),
        new BoolQuestion('In West Virginia, USA, if you accidentally hit an animal with your car, you are free to take it home to eat.', false,true),
    ];

    protected $mcQuestionBank = [];

    protected $textQuestionBank = [];

    protected $sectionBank = [
        $boolQuestionBank,
        $mcQuestionBank,
        $textQuestionBank
    ];

    public function nextQuestion(){
        if($this->questionNumber < sizeof($this->questionBank)){
            $this->questionNumber++;
        }
    }
    public function nextSection(){
        if($this->sectionNumber < sizeof($this->sectionBank)){
            $this->sectionNumber++;
        }
    }

    public function getQuestionText(){
        return $this->sectionBank[$this->sectionNumber][$this->questionNumber]->get_questionTitle();
    }

    public function getCorrectAnswer(){
        return$this->sectionBank[$this->sectionNumber][$this->questionNumber]->get_questionTextAnswer();
    }

    public function isSectionFinished(){
        if($this->questionNumber < sizeof($this->sectionBank[$this->sectionNumber])){
           return true;
        }else{
            return false;
        }
    }

    public function isFinished(){
        if($this->sectionNumber < sizeof($this->sectionBank[$this->sectionNumber])){
           return true;
        }else{
            return false;
        }
    }

    public function reset(){
        $this->questionNumber = 0;
        $this->sectionNumber = 0;
    }

    public function softReset(){
        $this->questionNumber = 0;
    }

}


?>